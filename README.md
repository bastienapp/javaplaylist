# Java - Gestionnaire de playlist

Mets en pratique la programmation orientée objet en Java

## Ressources

- [CodeCademy - Learn Java](https://www.codecademy.com/learn/learn-java)

## Contexte du projet

Ta mission est de créer un prototype de backend pour gérer les états d'une playlist audio : je précise que tu ne vas pas jouer les musiques (ça, c'est le frontend qui s'en chargera).

Tu vas devoir créer les classes suivantes, avec les attributs et méthodes attendus :

- Artist
  - firstName
  - lastName
  - getFullName() : retourne une chaîne contenant le nom et le prénom de l'artiste

- Music
  - title
  - duration : durée en secondes
  - artistSet : ensemble d'artistes
  - getInfos() : retourne une chaîne de caractères contenant le titre de la musique, sa durée au format mm:ss, et les noms et prénoms des artistes

- Playlist
  - currentMusic : musique en cours (peut être nulle)
  - musicList : liste de musiques (une même musique peut être ajoutée plusieurs fois)
  - add(Music music) : ajoute une musique à la liste
  - remove(int position) : retire une musique de la liste grâce à sa position dans la playlist
  - getTotalDuration() : calcule la durée totale de la playlist
  - next() : passe à la musique suivante dans la liste et la définit comme musique en cours

- Player
  - main() : ici tu créeras des instances des autres classes afin de tester leurs méthodes

## Modalités pédagogiques

En autonomie, sur une journée

## Modalités d'évaluation

Il sera évalué que :

- les classes, attributs et méthodes attendus sont bien créés
- la méthode `main` de la classe `Player` permet bien de tester toutes les classes et leurs méthodes

## Livrables

- Un lien vers un dépôt GitLab

## Critères de performance

- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions
