import java.util.ArrayList;

public class Playlist {

    private Music currentMusic;
    // ArrayList<Music> musicList; // null : ifférent d'une liste vide !
    private ArrayList<Music> musicList = new ArrayList<>(); // j'ai créé une liste vide !

    public void add(Music music) {
        musicList.add(music);
    }

    public void remove(Music music) {
        musicList.remove(music);
    }

    public void showPlaylist() {
        for (var music : musicList) {
            System.out.println(music.getInfos());
        }
        System.out.println(this.getTotalDuration());
    }

    public String getTotalDuration() {
        int total = 0;
        for (var music : musicList) {
            total += music.getDuration();
        }
        return Helper.formatDuration(total);
    }

    public void next() {

        if (this.musicList.size() > 0) {
            var first = this.musicList.get(0);
            this.currentMusic = first;
            this.remove(first);
            System.out.println("La musique jouée est " + this.currentMusic.getTitle());
        } else {
            System.out.println("La playlist est vide");
        }
    }
}
