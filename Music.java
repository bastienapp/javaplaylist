import java.util.HashSet;
import java.util.Set;

public class Music {

    private String title;
    private int duration;
    private HashSet<Artist> artistSet;

    public Music(String title, int duration, HashSet<Artist> artistSet) {
        this.title = title;
        this.duration = duration;
        this.artistSet = artistSet;
    }

    public String getTitle() {
        return this.title;
    }

    public int getDuration() {
        return this.duration;
    }

    public String formatDuration() {
        return Helper.formatDuration(this.duration);
    }

    public String getInfos() {
        // map en Java : stream
        String artistNames = "";
        for (var artist : artistSet) {
            artistNames += artist.getFullName() + "\n";
        }

        return this.title + " "
            + this.formatDuration() + "\n"
            + artistNames;
    }
}
