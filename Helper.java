public class Helper {

    public static String formatDuration(int duration) {
        int minuts = duration / 60;
        int seconds = duration % 60;

        return minuts + ":" + seconds;
    }
}
