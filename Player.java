import java.util.HashSet;

class Player {

    public static void main(String[] args) {
        Artist patoche = new Artist("Patrick", "Sébastien");
        Artist bastien = new Artist("Bastien", "Krettly");

        System.out.println(patoche.getFullName());

        // ensemble (Set) : un regroupement de données qui ne peuvent pas être en doublon
        HashSet<Artist> singers = new HashSet<>();
        singers.add(patoche);
        singers.add(bastien);

        Music lesSardines = new Music("Les Sardines", 235, singers);
        Music serviettes = new Music("Tourner les serviettes", 213, singers);

        System.out.println(lesSardines.getInfos());

        Playlist myList = new Playlist();
        myList.add(lesSardines);
        myList.add(serviettes);

        myList.showPlaylist();

        // myList.remove(lesSardines);

        System.out.println("__________");

        // myList.showPlaylist();

        myList.next();
        myList.next();
        myList.next();
    }
}